const parameters__list = document.querySelector(".parameters__list");
const parameters__controls = document.querySelector(".parameters__controls");
const canvas = document.querySelector(".canvas");

canvas.width = canvas.offsetWidth;
canvas.height = canvas.offsetHeight;

let graph = new Graph(canvas);

document.querySelector(".parameters__controls__add").addEventListener("click", () => {
  const newparam = document.createElement("input");

  newparam.setAttribute("type", "number");
  newparam.setAttribute("value", 0);
  newparam.setAttribute("min", 0);
  newparam.classList.add("parameter");
  newparam.addEventListener("change", reBuildGraph);

  parameters__list.appendChild(newparam);

  buildGraph();
});

document.querySelector(".parameters__controls__remove").addEventListener("click", () => {
  if (parameters__list.lastChild) {
    parameters__list.removeChild(parameters__list.lastChild);
    buildGraph();
  }
});

function buildGraph() {
  const vertexes = Array.from(parameters__list.childNodes).map(function (node) { return Number(node.value); });

  const restVertexes = graph.build(vertexes);
  checkRest(restVertexes);
}

function reBuildGraph() {
  const vertexes = Array.from(parameters__list.childNodes).map(function (node) { return Number(node.value); });

  const restVertexes = graph.rebuild(vertexes);
  checkRest(restVertexes);
}

function checkRest(restVertexes) {
  const isInvalid = restVertexes.some(el => el > 0);

  if (isInvalid) {
    parameters__controls.classList.remove("parameters__controls--valid");
    parameters__controls.classList.add("parameters__controls--invalid");
  } else {
    parameters__controls.classList.remove("parameters__controls--invalid");
    parameters__controls.classList.add("parameters__controls--valid");
  }

}

window.addEventListener('resize', () => {
  canvas.width = canvas.offsetWidth;
  canvas.height = canvas.offsetHeight;
  graph.redraw();
});
